{
  "schema_version": 1,
  "author": "KIT-CA Team <kit-ca-betrieb@scc.kit.edu>",
  "target_api": "4.1",
  "name": {
    "de": "Service-Account für ACME",
    "en": "Service Account for ACME"
  },
  "description": {
    "de": "Erstelle einen Service-Account für ACME4NETVS.",
    "en": "Create a service account for ACME4NETVS."
  },
  "list_display_name": {
    "de": "Service-Account für ACME anlegen in der OE {{ ou_short_name }} für den Dienst {{ name }}.",
    "en": "Create service account for ACME in the OU {{ ou_short_name }} for the service {{ name }}."
  },
  "display_variant": "create",
  "variables": {
    "ou_short_name": {
      "friendly_name": {
        "de": "OE-Name",
        "en": "OU Name"
      },
      "optional": false,
      "default": "",
      "nullable": false,
      "description": {
        "de": "Unter dieser OE wird ein Serviceaccount und eine Gruppe angelegt, der der Serviceaccount zugeordnet wird. Hierfür müssen Sie Betreuer der angegebenen OE sein.",
        "en": "Under this OU, a service account and a group will be created, which will be assigned to the service account. For this you must be a manager of the specified OU."
      },
      "type": "typeahead",
      "type_params": {
        "query": [
          {
            "idx": "own_mgr2ou_list",
            "name": "cntl.mgr2ou.list",
            "old": {
              "is_own": true
            }
          },
          {
            "idx": "unit_list",
            "name": "org.unit.list",
            "inner_join_ref": {
              "own_mgr2ou_list": "default"
            },
            "old": {
              "sorting_params_list": [
                "sub_position"
              ]
            }
          },
          {
            "idx": "unit_list_superset",
            "name": "org.unit.list",
            "inner_join_ref": {
              "unit_list": "api_func_org_unit_hierarchy_gfk_list_is_superset"
            }
          }
        ],
        "query_path": "unit_list_superset",
        "display_value": "short_name",
        "return_value": "short_name"
      }
    },
    "name": {
      "friendly_name": {
        "de": "Dienstname",
        "en": "Service Name"
      },
      "optional": false,
      "default": "",
      "nullable": false,
      "description": {
        "de": "Name des Dienstes. Hieraus werden automatisch die Dienstkennung des Serviceaccounts und der Gruppenname abgeleitet.",
        "en": "Name of the service. From this, the service identifier of the service account and the group name are automatically derived."
      },
      "type": "str"
    },
    "domains": {
      "friendly_name": {
        "de": "Domains",
        "en": "Domains"
      },
      "optional": false,
      "default": "",
      "nullable": false,
      "description": {
        "de": "Domains, für die Zertifikate bestellt werden können sollen. Subdomains dieser Domains sind automatisch mit eingeschlossen.",
        "en": "Domains for which certificates should be ordered. Subdomains of these domains are automatically included."
      },
      "list": true,
      "type": "fqdn"
    },
    "mgrs": {
      "friendly_name": {
        "de": "Verwaltende Accounts",
        "en": "Managing Accounts"
      },
      "optional": false,
      "default": "",
      "nullable": false,
      "description": {
        "de": "KIT-Accounts, die operativ für den Dienst zuständig sind. Diese können Aktionen im namen des Serviceaccounts ausführen.",
        "en": "KIT accounts that are operationally responsible for the service. These can perform actions on behalf of the service account."
      },
      "list": true,
      "type": "str"
    }
  },
  "transaction": [
    {
      "idx": "createMgr",
      "name": "cntl.mgr.create",
      "new": {
        "is_svc": true,
        "svc_id": "{{ ou_short_name }}:acme-{{ name }}"
      }
    },
    {
      "idx": "add_svc_to_ou",
      "name": "cntl.mgr2ou.create",
      "new": {
        "ou_short_name": "{{ ou_short_name }}"
      },
      "new_ref_params": [
        {
          "idx": "createMgr",
          "params": {
            "mgr_login_name": "login_name"
          }
        }
      ]
    },
    {
      "idx": "createGroup",
      "name": "cntl.group.create",
      "new": {
        "description": "ACME for {{ name }}",
        "name": "{{ ou_short_name }}-acme-{{ name }}",
        "ou_short_name": "{{ ou_short_name }}"
      }
    },
    {
      "idx": "svc_mgr2group",
      "name": "cntl.mgr2group.create",
      "new_ref_params": [
        {
          "idx": "createGroup",
          "params": {
            "group_name": "name"
          }
        },
        {
          "idx": "createMgr",
          "params": {
            "mgr_login_name": "login_name"
          },
          "join_type": "inner"
        }
      ]
    },
    {
      "idx": "tmpFqdnList",
      "name": "tmp.generic_object.list",
      "ref_params_join_on_val_attrs_tuple": ["item"],
      "old": {
        "_dict_list": "{{ domains }}"
      }
    },
    {
      "idx": "getFQDNS",
      "name": "dns.fqdn.list",
      "old": {
        "value_list": "{{ domains }}"
      }
    },
    {
      "idx": "tmpExistingFqdnList",
      "name": "tmp.generic_object.create",
      "ref_params_join_on_val_attrs_tuple": ["item"],
      "new_ref_params": [
        {"idx":  "getFQDNS", "params": {"item":  "value"}}
      ]
    },
    {
      "idx": "missingFqdns",
      "name": "tmp.generic_object.create",
      "ref_params_join_on_val_attrs_tuple": ["item"],
      "new_ref_params": [
        { "idx": "tmpFqdnList" },
        { "idx": "tmpExistingFqdnList",  "join_type": "full_anti", "join_on": "val" }
      ]
    },
    {
      "idx": "createfqdn",
      "name": "dns.fqdn.create",
      "new": {
        "type": "domain"
      },
      "new_ref_params": [
        {
          "idx": "missingFqdns",
          "params": {
            "value": "item"
          }
        }
      ]
    },
    {
      "idx": "fqdn2group",
      "name": "dns.fqdn2group.create",
      "new_ref_params": [
        {
          "idx": "createGroup",
          "params": {
            "group_name": "name"
          }
        },
        {
          "idx": "tmpFqdnList",
          "params": {
            "fqdn_value": "item"
          },
          "join_type": "cross"
        }
      ]
    },
    {
      "idx": "defineTmpMgrList",
      "name": "tmp.generic_object.list",
      "old": {
        "_dict_list": "{{ mgrs }}"
      }
    },
    {
      "idx": "mgrs2group",
      "name": "cntl.mgr2group.create",
      "new_ref_params": [
        {
          "idx": "defineTmpMgrList",
          "params": {
            "mgr_login_name": "item"
          },
          "join_type": "cross"
        },
        {
          "idx": "createGroup",
          "params": {
            "group_name": "name"
          }
        }
      ]
    }
  ],
  "returning": []
}
