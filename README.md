# netvs-hub

The NETVS Hub is a collection of custom defined transaction groups. The goal of transaction groups is to simplify 
processes, which consist of several interdependent transactions, as one transaction.

## Usage

All valid json templates contributed to this project get automatically included into NETVS.

Developers are able to enable the developer mode of NETVS and to load local JSON templates through an entry in the 
transaction list. Be careful: **Only load JSON templates from trusted sources!** Be aware that only valid JSON templates
will work and that they need to match the currently supported API specification to work. 

## Contribution Guide

You are welcome to contribute your own transaction groups!

Requirements:
- Python 3.6 or newer
- Internet connection

Here's a quick guide on the contribution of new JSON templates

1. Clone this repository to your local machine.
2. Create a new branch for your transaction group.
3. Start defining your JSON template in the `json_templates` folder in your text editor of your choice 
   (vim, Notepad++, ...) sticking to the latest released schema to ensure compatibility with the latest released 
   NETDB-API and NETVS. You can find all schema definitions in the `schemas` folder. For inspiration of JSON templates
   you can take a look into other JSON templates, but be careful with their `target_api` and `schema_version` as they
   might differ.
4. Check if your written JSON file is valid. This can be best done with the `netvs_hub_cli` python tool. You can install
   it with the following command (it's recommended to do this in a `venv`):
   ```shell
    pip3 install git+https://gitlab.kit.edu/scc-net/netvs/netvs_hub_cli.git@main#egg=netvs_hub_cli
    ```
   To run the checks execute the following command where you replace `<json_template>.json` with the file name of your 
   created template.
   ```shell
    python -m netvs_hub_cli --schemas schemas/ <json_template>.json
    ```
   Ensure, that there are no errors in your JSON template as the CI will test it again. Additionally, you can test your
   JSON template with the NETVS developer mode if the NETVS and NETDB-API version match.
5. Commit your added JSON template to the repository and push them.
6. Create a merge request to merge your added or modified JSON template onto the `prod` branch. After submission the 
   following requirements will be checked:
   - Successful CI run
   - Two successful reviews from the NETVS team
7. Your template will be merged into the main repo and will be deployed soon.

## License

The NETVS-Hub is licensed under the MIT-License. More information can be found [here](LICENSE).
